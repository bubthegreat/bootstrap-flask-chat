FROM ubuntu:xenial

ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

RUN apt-get update
RUN apt-get install -yqq git
RUN apt-get install -yqq python3-pip

COPY . /bootstrap-flask-chat/
RUN pip3 install -r /bootstrap-flask-chat/requirements.txt

ENV FLASK_APP=/bootstrap-flask-chat/chat.py

EXPOSE 5000

CMD ["flask", "run", "-p", "5000", "-h", "0.0.0.0"]
