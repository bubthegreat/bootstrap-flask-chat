Flask-SocketIO-Chat
===================

Forked from https://github.com/miguelgrinberg/Flask-SocketIO-Chat and modified to use bootstrap.  A simple chat application that demonstrates how to structure a Flask-SocketIO application.  


To install:
```
git clone https://gitlab.com/bubthegreat/bootstrap-flask-chat.git
cd bootstrap-flask-chat/
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```
To run
```
FLASK_APP=chat.py flask run -h 0.0.0.0 -p 5000
```